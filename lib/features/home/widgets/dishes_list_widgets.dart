import 'package:cached_network_image/cached_network_image.dart';
import 'package:favorite_button/favorite_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:shimmer/shimmer.dart';
import '../../../common/shimming_widgets.dart';
import '../../../controllers/home_controller.dart';
import '../../../repositories/models/dish_model.dart';


class DishesList extends StatelessWidget {
  const DishesList({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(builder: (controller) {
      return controller.dishesLoading
          ? _DishesLoading()
          :
      /// this is the list of dishes lists in home page
      ListView.builder(
        shrinkWrap: true,
        /// disable scrolling to make the scrolling on the top layer only
        physics: NeverScrollableScrollPhysics(),
        itemCount: controller.dishesLists.length,
        itemBuilder: (context , i){
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              _BuildListNameAndSeeALL(controller, i),
              SizedBox(height: 4.h,),
              _BuildDishesList(controller, i),
            ],
          );
        },

      );


    });
  }
/// this is an inner method to make the build function more readable
  Container _BuildDishesList(HomeController controller, int i) {
    return Container(
              height: 250,

              child: ListView(
                shrinkWrap: true,
                scrollDirection: Axis.horizontal,
                physics: BouncingScrollPhysics(),
                children: controller.dishesLists[i].dishes
                    .map((e) => DishWidget(dish: e))
                    .toList(),
              ),
            );
  }

  Row _BuildListNameAndSeeALL(HomeController controller, int i) {
    return Row(
              children: [
                Container(margin: EdgeInsets.only(left: 8.sp),child: Text(controller.dishesLists[i].name,style: TextStyle(fontWeight: FontWeight.bold,fontSize: 25.sp),)),
               Spacer(),

                IconButton(onPressed: (){}, icon: Icon(Icons.arrow_circle_right_outlined))

              ],
            );
  }

  Container _DishesLoading() => Container(height: 200,child: DishesLoading());
}

class DishWidget extends StatelessWidget {
  DishModel dish;
  var controller = Get.find<HomeController>();

  DishWidget({Key? key, required this.dish}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 300.w,
      // height: 90.h,
      margin: EdgeInsets.symmetric(horizontal: 6.0.w),
      child: Card(
        color: Theme.of(context).scaffoldBackgroundColor,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CachedNetworkImage(
              imageUrl: dish.imageUrl!,
              height: 130.h,
              width: 300.w,

              fit: BoxFit.cover,
              placeholder: (context, url) => SizedBox(
                height: 130.h,
                width: 300.w,
                child: Shimmer.fromColors(
                  baseColor: Colors.grey[300]!,
                  highlightColor: Colors.grey[100]!,
                  child: Container(),
                  period: Duration(milliseconds: 800),
                ),
              ),
            ),
            SizedBox(
              height: 2.h,
            ),
            _BuildDishNameAndFav(),

            /// This is the dish details
            Text(
              dish.distance!.toString() +
                  " mi" +
                  " . " +
                  dish.duration!.inMinutes.toString() +
                  " min" +
                  " . " +
                  dish.price.toString() +
                  " \$  " +
                  dish.dishBudge!,
              style: TextStyle(color: Colors.grey),
            ),
            SizedBox(height: 8.h,),
            Container(
              height: 15.h,
              child: RatingBar.builder(
                initialRating: dish.rating!+0.0,


                direction: Axis.horizontal,
                allowHalfRating: true,
                itemCount: 5,

                itemSize: 15.0,
                itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                itemBuilder: (context, _) => Icon(
                  Icons.star,
                  color: Colors.amber,
                ), onRatingUpdate: (double value) {  },

              ),
            ),
          ],
        ),
      ),
    );
  }

  Row _BuildDishNameAndFav() {
    return Row(
            children: [
              Text(
                dish.name ?? "",
                style: TextStyle(fontWeight: FontWeight.w600),
              ),
              Spacer(),
              Container(
                height: 20.h,
                width: 20.w,
                margin: EdgeInsets.only(right: 8.w, top: 8.h),
                child: FavoriteButton(
                  iconSize: 25.r,
                  isFavorite: dish.isFav,
                  // iconDisabledColor: Colors.white,
                  valueChanged: (_isFavorite) {
                    dish.isFav = _isFavorite;
                    controller.update();
                  },
                ),
              ),
            ],
          );
  }
}

