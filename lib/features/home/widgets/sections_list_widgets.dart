

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import '../../../common/custom_image_view.dart';
import '../../../common/shimming_widgets.dart';
import '../../../controllers/home_controller.dart';
import '../../../repositories/models/section_model.dart';




/// The top icons list in Home page
class SectionsList extends StatefulWidget {
  const SectionsList({
    super.key,
  });

  @override
  State<SectionsList> createState() => _SectionsListState();
}

class _SectionsListState extends State<SectionsList> with SingleTickerProviderStateMixin{
  late AnimationController _animationController;
  late Animation<double> _animation;

  @override
  void initState() {
    // Create an AnimationController with duration of 1 second
    _animationController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 750),
    );

    // Create a Tween to define the animation from 0.0 to 1.0
    _animation = Tween<double>(begin: 0.0, end: 1.0).animate(
      CurvedAnimation(parent: _animationController, curve: Curves.easeIn),
    );

    // Start the animation
    _animationController.forward();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(builder: (controller) {
    return  AnimatedBuilder(
        animation: _animationController,
        builder: (context, child) {
          return( controller.sectionLoading
          ? SectionLoading()
          : ListView.builder(

        scrollDirection: Axis.horizontal,
        itemCount: controller.sections.length,
        itemBuilder: (context ,i){

          return Opacity(
            opacity: _animation.value,
            child: Transform.translate(
              offset: Offset((1 - _animation.value) * 100, 0),
              child: SectionWidget(section: controller.sections[i]),
            ),
          );
        }
            ,
      ));},
      );
    });
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }
}


/// section icon with name in home page
class SectionWidget extends StatelessWidget {
  SectionModel section;
  var controller = Get.find<HomeController>();
  SectionWidget({Key? key, required this.section}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 6.0.w),
      child: Column(
        children: [
          InkWell(
            child: Container(
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                border: Border.all(
                  color:  Theme.of(context).primaryColor.withOpacity(section.id ==controller.selectedSection?1:0),
                  width:   3.0,
                ),
              ),
              child: CustomCircleImage(
                imageUrl: section.imageUrl,
                local: true,
              ),
            ),
            onTap: (){
              Get.find<HomeController>().changeSelectedSection(id:section.id!);
            },
          ),
          SizedBox(
            height: 2.h,
          ),
          Text(
            section.name ?? "",
            style: TextStyle(fontWeight: FontWeight.bold,color: section.id ==controller.selectedSection?Theme.of(context).primaryColor:Theme.of(context).textTheme.titleMedium!.color),
          )
        ],
      ),
    );
  }
}


