

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../common/custom_image_view.dart';
import '../../../common/shimming_widgets.dart';
import '../../../controllers/home_controller.dart';
import '../../../repositories/models/category_model.dart';

/// This is the list that's appears in the home page
class CategoriesList extends StatelessWidget {
  const CategoriesList({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(builder: (controller) {
      return controller.categoryLoading
          ? SectionLoading()
          : ListView(
        scrollDirection: Axis.horizontal,
        children: controller.categories
            .map((e) => CategoryWidget(category: e))
            .toList(),
      );
    });
  }
}


/// This is the category card
class CategoryWidget extends StatelessWidget {
  CategoryModel category;
  var controller = Get.find<HomeController>();
  CategoryWidget({Key? key, required this.category}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 6.0.w),
      child: Column(
        children: [
          InkWell(
            child: Container(
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                border: Border.all(
                  color:  Theme.of(context).primaryColor.withOpacity(category.id ==controller.selectedCategory?1:0),
                  width:   3.0,
                ),
              ),
              child: CustomCircleImage(
                imageUrl: category.imageUrl,
                backgroundColor: Colors.white.withOpacity(0),
                local: true,
              ),
            ),
            onTap: (){
              Get.find<HomeController>().changeSelectedCategory(id:category.id!);
            },
          ),
          SizedBox(
            height: 2.h,
          ),
          Text(
            category.name ?? "",
            style: TextStyle(fontWeight: FontWeight.bold,color: category.id ==controller.selectedCategory?Theme.of(context).primaryColor:Theme.of(context).textTheme.titleMedium!.color),
          )
        ],
      ),
    );
  }
}
