import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:shimmer/shimmer.dart';
import 'package:untitled2/features/home/widgets/dishes_list_widgets.dart';
import '../../../common/custom_image_view.dart';
import '../../../common/shimming_widgets.dart';
import '../../../controllers/home_controller.dart';
import '../../../repositories/models/banner_model.dart';

class BannerWidget extends StatelessWidget {
  BannerModel banner;
  var controller = Get.find<HomeController>();

  BannerWidget({Key? key, required this.banner}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 6.0.w),
      child: CustomImage(
        imageUrl: banner.imageUrl,
      ),
    );
  }
}

class BannersList extends StatelessWidget {
  const BannersList({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(builder: (controller) {
      return controller.bannersLoading
          ? DishesLoading()
          : CarouselSlider(
              items: controller.banners!
                  .map((e) => BannerWidget(banner: e))
                  .toList(),
              options: CarouselOptions(
                height: 150.h,
                aspectRatio: 16 / 9,
                viewportFraction: 0.8,
                initialPage: 0,
                enableInfiniteScroll: true,
                reverse: false,
                autoPlay: true,
                autoPlayInterval: Duration(seconds: 3),
                autoPlayAnimationDuration: Duration(milliseconds: 800),
                autoPlayCurve: Curves.fastOutSlowIn,
                enlargeCenterPage: true,
                enlargeFactor: 0.3,
                scrollDirection: Axis.horizontal,
              ));
    });
  }
}


