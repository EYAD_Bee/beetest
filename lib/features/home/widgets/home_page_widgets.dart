import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

/// This widget is the top of the home page
class HeaderWidget extends StatelessWidget {
  const HeaderWidget({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 20.sp, horizontal: 10.sp),
        child: Row(
          children: [
            Row(
              children: [
                Icon(
                  Icons.pin_drop,
                  size: 17.r,
                ),
                SizedBox(
                  width: 3.sp,
                ),
                Text(
                  "Damascus",
                  style:
                  TextStyle(fontWeight: FontWeight.bold, fontSize: 18.sp),
                ),
                SizedBox(
                  width: 3.sp,
                ),
                RotatedBox(
                    quarterTurns: 1,
                    child: Icon(
                      Icons.arrow_forward_ios_rounded,
                      size: 15.r,
                    ))
              ],
            ),
            Spacer(),
            IconButton(onPressed: () {}, icon: Icon(Icons.shopping_cart))
          ],
        ),
      ),
    );
  }
}