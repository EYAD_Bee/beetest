

import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../common/range_picker.dart';
import '../../../common/shimming_widgets.dart';
import '../../../controllers/home_controller.dart';
import '../../../repositories/models/filter_model.dart';

class FiltersList extends StatefulWidget {
   FiltersList({
    super.key,
  });

  @override
  State<FiltersList> createState() => _FiltersListState();
}

class _FiltersListState extends State<FiltersList> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(builder: (controller) {
      return controller.filtersLoading
          ? FiltersLoading()
          :
      /// I made it without builder because I need to render them all and once
      ListView(
        scrollDirection: Axis.horizontal,
        children:controller.filters
            .map((e) => Container(child: FilterWidget(filter: e)))
            .toList()
        /// Here I have added a button to clear filters
          ..add(Container(child: TextButton(onPressed: (){
              controller.clearFilters();
        }, child: Text("Clear")),)),
      );
    });
  }
}



class FilterWidget extends StatelessWidget {
  FilterModel filter;
  var controller = Get.find<HomeController>();
  FilterWidget({Key? key, required this.filter}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 6.0.w),
      child: Column(
        children: [
          InkWell(
            child: Container(
              decoration: BoxDecoration(
                 shape: BoxShape.rectangle,
                border: Border.all(
                  color:  Theme.of(context).primaryColor.withOpacity(filter.id ==controller.selectedFilter?1:0),
                  width:   3.0,
                ),
                borderRadius: BorderRadius.circular(20.r),
                color: Theme.of(context).backgroundColor
              ),
               child: Padding(
                 padding: const EdgeInsets.symmetric(horizontal: 4.0 ,vertical: 2.0),
                 child: Row(
                   children: [
                     Text(
                        filter.name ?? "",
                        style: TextStyle(fontWeight: FontWeight.bold,),
                      ),

                     /// The bottom arrow not showing in filter without additional data
                     if(filter.filterType != FilterType.Empty)
                       Container(
                         margin: EdgeInsets.only(left: 4.w),
                         child:  RotatedBox(
                               quarterTurns: 1,
                               child: Icon(

                                 Icons.arrow_forward_ios_rounded,
                                 size: 12.r,
                               )),

                         ),

                   ],
                 ),
               )

            ),
            onTap: ()async{
              if(filter.filterType == FilterType.Range){
                 showPriceRangeDialog(context);
              }
              else if(filter.filterType == FilterType.Rating){
                showRatingDialog(context);
              }

            },
          ),

        ],
      ),
    );
  }

  Future<dynamic> showRatingDialog(BuildContext context) {
    return showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    title: Text('Choose a rate'),
                    content: RatingBar.builder(
                      initialRating: 3,
                      minRating: 1,
                      direction: Axis.horizontal,
                      allowHalfRating: true,
                      itemCount: 5,

                      itemSize: 40.0,
                      itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                      itemBuilder: (context, _) => Icon(
                        Icons.star,
                        color: Colors.amber,
                      ),
                      onRatingUpdate: (rating) {
                        filter.value = rating.round();
                      },
                    ),
                    actions: <Widget>[
                      TextButton(
                        child: Text('Cancel'),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                      TextButton(
                        child: Text('Submit'),
                        onPressed: () {


                          Get.find<HomeController>().changeSelectedFilter(filter: filter,id:filter.id!);
                          Navigator.of(context).pop();
                        },
                      ),
                    ],
                  );
                },
              );
  }

  Future<void> showPriceRangeDialog(BuildContext context) async {
    await showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(

          title: Text('Choose a price Range'),
          content: RangeSilderDialog(filter:filter),
          actions: <Widget>[

            Center(
              child: TextButton(
                child: Text('Ok'),
                onPressed: () {
                  Get.find<HomeController>().changeSelectedFilter(filter: filter,id:filter.id!);

                  Navigator.of(context).pop();
                },
              ),
            ),
          ],
        );
      },
    );
  }
}



