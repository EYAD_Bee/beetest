import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:untitled2/controllers/home_controller.dart';
import 'package:untitled2/features/home/widgets/banners_list_widgets.dart';
import 'package:untitled2/features/home/widgets/filters_widgets.dart';

import 'widgets/cateories_list_widgets.dart';
import 'widgets/dishes_list_widgets.dart';
import 'widgets/home_page_widgets.dart';
import 'widgets/sections_list_widgets.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  var homeController = Get.find<HomeController>();

  @override
  void initState() {
    /// This function is for load the needed lists
    homeController.init();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GetBuilder<HomeController>(builder: (controller) {
        return SingleChildScrollView(
          child: Column(
            children: [
              /// Location and Cart Section
              HeaderWidget(),

              /// Sections List
              Container(
                height: 100,
                child: SectionsList(),
              ),
              Divider(
                height: 3.h,
                thickness: 2.h,
              ),
              SizedBox(
                height: 12.h,
              ),

              /// Categories List
              if (controller.categories.isNotEmpty)
                Container(
                  height: 100,
                  child: CategoriesList(),
                ),

              SizedBox(
                height: 12.h,
              ),

              /// Filters List
              Container(
                height: 30.h,
                child: FiltersList(),
              ),

              // SizedBox(height: 12.h,),

              ///Banners
              Container(
                height: 200.h,
                child: BannersList(),
              ),
              SizedBox(
                height: 12.h,
              ),

              /// Dishes List

              DishesList(),
            ],
          ),
        );
      }),
    );
  }
}






