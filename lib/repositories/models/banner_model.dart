import 'package:untitled2/repositories/models/base_model.dart';

class BannerModel extends BaseModel{
  String imageUrl;

  BannerModel({
    required this.imageUrl,
    required int id,

  }):super(id: id,name: "");
}