class SectionModel {
  int? id;
  String? imageUrl;
  String? name;

  SectionModel({
    this.id,
    this.imageUrl,
    this.name,
  });

  Map<String, dynamic> toMap() {
    return {
      'id': this.id,
      'imageUrl': this.imageUrl,
      'name': this.name,
    };
  }

  factory SectionModel.fromMap(Map<String, dynamic> map) {
    return SectionModel(
      id: map['id'] as int,
      imageUrl: map['imageUrl'] as String,
      name: map['name'] as String,
    );
  }
}