


import 'package:untitled2/repositories/models/base_model.dart';

class FilterModel extends BaseModel{

  FilterType filterType ;
  dynamic value;

  FilterModel({
    required this.filterType,
   required String name,
   required int id,
  }):super(id: id, name: name);
}


enum FilterType{
  Empty, List, Range, Rating
}