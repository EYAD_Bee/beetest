



import 'section_model.dart';

class CategoryModel extends SectionModel {

  int? sectionId;

  CategoryModel({
    this.sectionId,
    required int id,
    required String imageUrl,
    required String name,
  }):super(
    id:id,
    imageUrl: imageUrl,
    name : name,);


}