class BaseModel {
  int? id;
  String? name;

  BaseModel({
    this.id,
    this.name,
  });

  Map<String, dynamic> toMap() {
    return {
      'id': this.id,
      'name': this.name,
    };
  }

  factory BaseModel.fromMap(Map<String, dynamic> map) {
    return BaseModel(
      id: map['id'] as int,
      name: map['name'] as String,
    );
  }
}