

import 'package:untitled2/repositories/models/base_model.dart';

class DishModel extends BaseModel {

  String? imageUrl;
  num? distance;
  num? price;
  int? rating;
  bool? isFav;
  Duration? duration;
  String? dishBudge;

  DishModel({
    this.imageUrl,
    this.distance,
    this.price,
    this.rating,
    this.isFav,
    this.duration,
    this.dishBudge,
    required int id,
    required String name
  }) :super(id: id, name: name);

}

class DishesListModel{
  List<DishModel> dishes;
  String name;

  DishesListModel({
    required this.dishes,
    required this.name,
  });
}



enum DishBudge{
  FreeDelivery , In15Minutes
}