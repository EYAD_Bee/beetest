import 'package:untitled2/repositories/models/banner_model.dart';
import 'package:untitled2/repositories/models/dish_model.dart';
import 'package:untitled2/repositories/models/filter_model.dart';

import 'models/category_model.dart';
import 'models/section_model.dart';
import 'static_lists.dart';


/// This repository is responsible for all methode called in home page
class HomeRepository {
  Future<List<SectionModel>> sectionsList() async {
    return StaticData.sectionsList;
  }

  Future<List<CategoryModel>> categoriesList() async {
    await Future.delayed(Duration(seconds: 2));
    return StaticData.categoriesList;
  }

  Future<List<FilterModel>> filtersList() async {
    await Future.delayed(Duration(seconds: 2));
    return StaticData.filtersList;
  }

  Future<List<DishesListModel>> dishesList() async {
    await Future.delayed(Duration(seconds: 2));
    return [DishesListModel(dishes: StaticData.dishesList, name: "New Dishes"),
      DishesListModel(dishes: StaticData.dishesList, name: "Trending")
    ];
  }

  Future<List<BannerModel>> bannersList() async {
    await Future.delayed(Duration(seconds: 2));
    return StaticData.BannersList;
  }
}
