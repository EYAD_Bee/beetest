import 'package:untitled2/repositories/models/banner_model.dart';
import 'package:untitled2/repositories/models/base_model.dart';
import 'package:untitled2/repositories/models/dish_model.dart';
import 'package:untitled2/repositories/models/filter_model.dart';

import 'models/category_model.dart';
import 'models/section_model.dart';



/// This class holds a static data for testing
class StaticData {

  static List<SectionModel> sectionsList = [
    SectionModel(
        name: "Grocery",
        imageUrl: "assets/food.png",
        id :1
    ),

    SectionModel(
        name: "Convenience",
        imageUrl: "assets/cleaning.png",
        id :2
    ),



    SectionModel(
        name: "Gifts",
        imageUrl: "assets/noodles.png",
        id :3
    ),
    SectionModel(
        name: "Catering",
        imageUrl: "assets/meat.png",
        id :4
    ),
  ];

  static List<BannerModel> BannersList = [
    BannerModel(
        imageUrl: "https://www.credencys.com/wp-content/uploads/2020/01/on-demand-food-delivery-app-banner.jpg",
        id :1
    ),

    BannerModel(
        imageUrl: "https://assets.sharis.com/wp-content/uploads/2019/01/19221206/doordash-banner2.jpg",
        id :2
    ),
  ];

  static List<CategoryModel> categoriesList = [
    CategoryModel(
        name: "Breakfast",
        imageUrl: "assets/diet.png",
        id :1,
        sectionId: 1
    ),

    CategoryModel(
        name: "Mexican",
        imageUrl: "assets/taco.png",
        id :2,
        sectionId: 1

    ),
    CategoryModel(
        name: "Cafe",
        imageUrl: "assets/cafe.png",
        id :3,
        sectionId: 1
    ),
    CategoryModel(
        name: "Shawarma",
        imageUrl: "assets/shawarma.png",
        id :4,
        sectionId: 2

    ),
    CategoryModel(
        name: "Organic",
        imageUrl: "assets/organic-food.png",
        id :4,
        sectionId: 1
    ),
    CategoryModel(
        name: "Vegetables",
        imageUrl: "assets/diet.png",
        id :4,
        sectionId: 1
    ),
  ];

  static List<FilterModel> filtersList = [
    FilterModel(
        name: "Offers",
        id :1,
      /// Empty Refer to it only clickable without additional paramters
      filterType:  FilterType.Empty
    ),

    FilterModel(
        name: "PickUp",
        id :2,
        filterType:  FilterType.Empty
    ),

    FilterModel(
        name: "Rating",
        id :3,
        filterType:  FilterType.Rating
    ),

    FilterModel(
        name: "Price",
        id :4,
        filterType:  FilterType.Range
    ),
  ];


  static List<DishModel> dishesList = [
    DishModel(
        name: "French Fries",
        id :1,
      imageUrl: "https://www.tastingtable.com/img/gallery/frozen-french-fry-tricks-youll-wish-you-knew-sooner/l-intro-1662484314.jpg"
,rating: 3,
      isFav: false,
      duration: Duration(minutes: 30),
      dishBudge: "Free Delivery",
      distance: 1.2,
      price: 50
    ),
    DishModel(
        name: "Pizza",
        id :2,
        imageUrl: "https://www.daysoftheyear.com/wp-content/uploads/pizza-day.jpg"
        ,rating: 1,
        isFav: true,
        duration: Duration(minutes: 20),
        dishBudge: "Free Delivery",
        distance: 1.2,
        price: 1
    ),
    DishModel(
        name: "Shwarwm",
        id :3,
        imageUrl: "https://cdn.broadsheet.com.au/melbourne/images/2019/10/29/174028-4533-172348-6918-Shwarmama_HDolby-IMG_2047_dVk4gsi.jpg"
        ,rating: 5,
        isFav: false,
        duration: Duration(minutes: 45),
        dishBudge: "Free Delivery",
        distance: 1.2,
        price: 100
    ),


  ];
}
