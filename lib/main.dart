import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:untitled2/controllers/home_controller.dart';
import 'package:untitled2/features/app_view.dart';

void main() {

  /// this method is for init controllers
  initializeControllers();
  runApp(AppInitial());
}

void initializeControllers(){
  Get.put(HomeController());
}


class AppInitial extends StatefulWidget {
  const AppInitial({Key? key}) : super(key: key);

  @override
  State<AppInitial> createState() => _AppInitialState();
}

class _AppInitialState extends State<AppInitial> {
  @override
  Widget build(BuildContext context) {
    return  ScreenUtilInit(
      designSize: const Size(360, 690),
      minTextAdapt: true,
      splitScreenMode: true,
      builder: (context , child) {
        return MaterialApp(
          debugShowCheckedModeBanner: false,
          theme: ThemeData(
            // brightness: Brightness.light,
            // textTheme: Typography.englishLike2018.apply(fontSizeFactor: 1.sp),
            // scaffoldBackgroundColor: Colors.grey[900],

          primaryColor: Colors.red,
            primarySwatch: Colors.red,
          ),
          home: child,
        );
      },
      child:  AppView(),
    );
  }
}



