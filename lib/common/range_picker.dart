
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../controllers/home_controller.dart';
import '../repositories/models/filter_model.dart';

class RangeSilderDialog extends StatefulWidget {
  FilterModel filter;
  RangeSilderDialog({super.key, required  this.filter});

  @override
  State<RangeSilderDialog> createState() => _RangeSilderDialogState();
}

class _RangeSilderDialogState extends State<RangeSilderDialog> {
  RangeValues _currentRangeValues = const RangeValues(40, 80);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 100,
      width: 200,
      child: RangeSlider(
        values: _currentRangeValues,
        max: 100,
        min: 0,
        divisions: 5,
        labels: RangeLabels(
          _currentRangeValues.start.round().toString(),
          _currentRangeValues.end.round().toString(),

        ),
        onChanged: (RangeValues values) {
          setState(() {
            _currentRangeValues = values;
            widget.filter.value = _currentRangeValues;

            Get.find<HomeController>().changeSelectedFilter(filter: widget.filter,id: widget.filter.id!
            );
          });
        },
      ),
    );
  }
}

