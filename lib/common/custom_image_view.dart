

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:shimmer/shimmer.dart';

class CustomCircleImage extends StatelessWidget {
  String? imageUrl;
  Color? backgroundColor;
  /// This field to switch between local and network  images
  bool local;

  CustomCircleImage(
      {Key? key, this.imageUrl, this.backgroundColor, this.local = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CircleAvatar(
        backgroundColor: backgroundColor ?? Colors.grey[200],
        // backgroundImage: local
        //     ? AssetImage(imageUrl!,) as ImageProvider
        //     : NetworkImage(imageUrl ??
        //     "https://static.vecteezy.com/system/resources/thumbnails/004/141/669/small/no-photo-or-blank-image-icon-loading-images-or-missing-image-mark-image-not-available-or-image-coming-soon-sign-simple-nature-silhouette-in-frame-isolated-illustration-vector.jpg"),

        child:local
    ? Image.asset(imageUrl!,height: 40.h,width: 40.h,)
        : Image.network(imageUrl! ) ,radius: 25.r);
  }
}

class CustomImage extends StatelessWidget {
  String imageUrl;

  CustomImage({Key? key, required this.imageUrl}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      imageUrl: imageUrl,
      height: 130.h,
      width: 300.w,
      fit: BoxFit.cover,
      placeholder: (context, url) => SizedBox(
        height: 130.h,
        width: 300.w,
        child: Shimmer.fromColors(
          baseColor: Colors.grey[300]!,
          highlightColor: Colors.grey[100]!,
          child: Container(),
          period: Duration(milliseconds: 800),
        ),
      ),
    );
  }
}