
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:shimmer/shimmer.dart';

/// Shimming for top icons in home pages
class SectionLoading extends StatelessWidget {
  const SectionLoading({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      scrollDirection: Axis.horizontal,
      itemCount: 5,
      itemBuilder: (BuildContext context, int index) {
        return Column(
          children: [
            Shimmer.fromColors(
              baseColor: Colors.grey[300]!,
              highlightColor: Colors.grey[100]!,
              child: Container(
                margin: EdgeInsets.symmetric(horizontal: 6.0.w),
                child: CircleAvatar(
                  radius: 30.r,
                  backgroundColor: Colors.grey[300],
                ),
              ),
            ),
            SizedBox(height: 6.0.h),
            Shimmer.fromColors(
              baseColor: Colors.grey[300]!,
              highlightColor: Colors.grey[100]!,
              child: Container(
                height: 12.0.h,
                width: 50.0.w,
                color: Colors.grey[300],
              ),
            ),
          ],
        );
      },
    );
  }
}



class FiltersLoading extends StatelessWidget {
  const FiltersLoading({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      scrollDirection: Axis.horizontal,
      itemCount: 5,
      itemBuilder: (BuildContext context, int index) {
        return Column(
          children: [

            Container(

              margin: EdgeInsets.symmetric(horizontal: 8.sp),
              child: Shimmer.fromColors(
                baseColor: Colors.grey[300]!,
                highlightColor: Colors.grey[100]!,
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8.0.r),
                    color: Colors.grey[300],
                  ),
                  height: 12.0.h,
                  width: 50.0.w,

                ),
              ),
            ),
          ],
        );
      },
    );
  }
}


/// Dishes Shimming
class DishesLoading extends StatelessWidget {
  const DishesLoading({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      scrollDirection: Axis.horizontal,
      itemCount: 5,
      itemBuilder: (BuildContext context, int index) {
        return Column(
          children: [
            Shimmer.fromColors(
              baseColor: Colors.grey[300]!,
              highlightColor: Colors.grey[100]!,
              child: Container(
                margin: EdgeInsets.symmetric(horizontal: 6.0.w),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(10.0),
                  child: Container(
                    height: 150.h,
                    width: 200.w,
                    color: Colors.grey[300],
                  ),
                ),
              ),
            ),
            SizedBox(height: 6.0.h),
            Shimmer.fromColors(
              baseColor: Colors.grey[300]!,
              highlightColor: Colors.grey[100]!,
              child: Container(
                height: 12.0.h,
                width: 50.0.w,
                color: Colors.grey[300],
              ),
            ),
          ],
        );
      },
    );
  }
}
