

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:untitled2/repositories/models/banner_model.dart';
import 'package:untitled2/repositories/models/dish_model.dart';
import 'package:untitled2/repositories/models/filter_model.dart';

import '../repositories/home_repository.dart';
import '../repositories/models/category_model.dart';
import '../repositories/models/section_model.dart';

class HomeController extends GetxController{

  /// -1 means that there is no selected section
  int selectedSection = -1;
  List<SectionModel> sections =[];
  bool sectionLoading = false;

  List<CategoryModel> categories =[];
  bool categoryLoading = false;
  int selectedCategory = -1;

  List<FilterModel> filters =[];
  bool filtersLoading = false;
  int selectedFilter = -1;

  ///This list to store the old values , for revert filtering
  List<DishesListModel> oldDishesLists  = [];
  List<DishesListModel> dishesLists  = [];
  bool dishesLoading = false;


  List<BannerModel>? banners ;
  bool bannersLoading = false;



    HomeRepository homeRepository = HomeRepository();

  void init() async{
    getSections();
    getFilters();
    getDishes();
    getBanners();

  }

  /// all the get methods load data from Repository and the update loading status
  void getSections()async{
    sectionLoading  =true;
    update();
    sections = await homeRepository.sectionsList();
    sectionLoading  =false;
    update();
  }
  void getBanners()async{
    bannersLoading  =true;
    update();
    banners = await homeRepository.bannersList();
    bannersLoading  =false;
    update();
  }
  void getFilters()async{
    filtersLoading  =true;
    update();
    filters = await homeRepository.filtersList();
    filtersLoading  =false;
    update();
  }

  void getDishes()async{
    dishesLoading  =true;
    update();
    dishesLists = await homeRepository.dishesList();
    oldDishesLists = dishesLists;
    dishesLoading  =false;
    update();
  }

  void getCategories()async{
    categoryLoading  =true;
    update();
    categories = await homeRepository.categoriesList();
    categories = categories.where((element) => element.sectionId == selectedSection).toList();
    categoryLoading  =false;
    update();
  }

  void changeSelectedSection({required int id}) {
    if(selectedSection == id){
      selectedSection = -1;
    }
    else{
      selectedSection = id;
    }
    /// Reload the categories based on section that have chosen
    getCategories();
    update();

  }

  void changeSelectedCategory({required int id}) {
    if(selectedCategory == id){
      selectedCategory = -1;
      getDishes();
    }
    else{
      selectedCategory = id;
    }
    update();

  }

  /// This method is for filtering the dishes in the home page based on filter selected
  void changeSelectedFilter({ required FilterModel filter,required int id}) {
    if(selectedFilter == id){
      selectedFilter = -1;
      clearFilters();
    }
    else{
      selectedFilter = id;
    }
    if(filter.filterType == FilterType.Rating){
      /// Loop for each dish in the dishes list if it higher than selected rate
      for (var element in dishesLists) {
        element.dishes = element.dishes.where((element) => element.rating! >= (filter.value as int ) ).toList();
      }
    }
    else if(filter.filterType == FilterType.Range){
      /// Loop for each dish in the dishes list if it in between the price range

      for (var element in dishesLists) {
        element.dishes = element.dishes.where((element) => element.price! >= (filter.value as RangeValues ).start
            &&element.price! <= (filter.value as RangeValues ).end ).toList();
      }
    }
    update();

  }
  /// Reset Dish lists
  void clearFilters() {
    selectedFilter = -1;
    getDishes();
    update();
  }



}